#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2013-2022 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT
# Spectral data handling

import logging

import scipy.interpolate
import numpy as np

from typing import *

logger = logging.getLogger(__name__)


def create_mapping(wl: Sequence[float], spd: Sequence[float]):
	"""
	:return: interpolator returning spd data at a wavelength, with __call__
	"""
	mapping = scipy.interpolate.interp1d(wl, spd,
	 bounds_error=False,
	 fill_value="extrapolate",
	)
	return mapping


def spectrum_resample(wl_src, val_src, wl_dst):
	mapping = create_mapping(wl_src, val_src)
	if wl_dst[0] < wl_src[0] or wl_dst[-1] > wl_src[-1]:
		logger.warning("Resampling beyond source range, was %s-%s but want %s-%s",
		 wl_src[0], wl_src[-1], wl_dst[0], wl_dst[-1])
	return mapping(wl_dst)


class SpectrumResampler:
	"""
	"""
	def __init__(self, wl_src, wl_dst):
		"""
		:param wl_src: wavelengths in source domain
		:param wl_dst: wavelengths in destination domain
		"""
		self.wl_src = np.array(wl_src)
		self.wl_dst = np.array(wl_dst)
		if wl_dst[0] < wl_src[0] or wl_dst[-1] > wl_src[-1]:
			logger.warning("Resampling beyond source range, was %s-%s but want %s-%s",
			 wl_src[0], wl_src[-1], wl_dst[0], wl_dst[-1])

	def __call__(self, vals):
		"""
		:param vals: np array, same shape as wl_src
		"""
		wl_src = self.wl_src
		wl_dst = self.wl_dst
		vals = np.array(vals)
		assert vals.shape == wl_src.shape
		mapping = create_mapping(wl_src, vals)
		return mapping(wl_dst)

