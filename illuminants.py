#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2015-2022 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT
# Data for illuminants

import io
import logging

import numpy as np

from .datafiles import find_file
from .hyperspectral import create_mapping


logger = logging.getLogger(__name__)


class Illuminants_CIE_15_2004():
	"""
	Illuminant SPD from
	`CIE 15: Technical Report: Colorimetry, 3rd edition
	<https://archive.org/details/gov.law.cie.15.2004>`_
	§ 11.1 Table T1: Relative spectral power distributions of CIE illuminants
	"""
	def __init__(self):
		path = find_file("spectrums/CIE.15.2004.T1.txt")
		assert path is not None
		with io.open(path, "r") as f:
			self.names = names = f.readline().split(';')[1:]

			data = np.loadtxt(
			 f,
			 delimiter=';',
			)

			self.spds = spds = { name: list() for name in names}
			self.wavelengths = wavelengths = []
			for idx_wl, (wl, *values) in enumerate(data):
				wavelengths.append(wl)
				for name, value in zip(names, values):
					spds[name].append(value)
			assert idx_wl+1 == len(wavelengths)

	def spd_for_illuminant(self, name, normalize=False):
		"""
		:return: callable returning SPD of its argument
		"""
		wavelengths = self.wavelengths
		spd = np.array(self.spds[name])
		logger.debug("%s: %s", name, spd.tolist())

		if normalize:
			spd /= spd.max()

		return create_mapping(wavelengths, spd)
